# Gobsly Location

Javascript SDK for Gobsly Location. 

## Installation

```bash
npm install @gobsly/location

or

yarn add @gobsly/location
```

## Usage

Get an API key from [Gobsly Location](https://gobsly.com).

```javascript
import { init, getLocation } from "@gobsly/location";

init("YOUR_API_KEY");

const app = async () => {
  const userLocation = await getLocation();

  console.log(userLocation);
};

app();
```

## About us

Gobsly Location is the easiest way to adapt to the user's location. 

[Visit us.](https://gobsly.com)

## License

[MIT](https://choosealicense.com/licenses/mit/)