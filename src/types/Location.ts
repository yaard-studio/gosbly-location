export type Location = {
  capital: string | null;
  city: string | null;
  continentName: string | null;
  countryCode: string | null;
  countryName: string | null;
  currencyCode: string | null;
  ipAddress: string | null;
  latitude: number | null;
  longitude: number | null;
  phoneCode: string | null;
  population: number | null;
};
