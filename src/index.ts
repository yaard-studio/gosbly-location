import { Location } from "./types/Location";

let apiKey: string | undefined = undefined;

export const init = (key: string) => {
  apiKey = key;
};

export const getLocation = async () => {
  if (!apiKey) throw new Error("API key is not set");

  const response = await fetch(
    `https://api.gobsly.com/location/v1/?apiKey=${apiKey}`,
    {
      method: "GET",
    }
  );

  const result: Location = await response.json();

  return result;
};
